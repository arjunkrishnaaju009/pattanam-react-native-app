import React, { Component } from 'react';
import { View, Text,StyleSheet,TextInput,Image ,TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
        text1: 'First Name',
        text2: 'Email',
        text3: 'Password',
        text4: 'Confirm Password',
        text5: 'Phone Number',
        text6: 'Hometown'
    };
  }

  render() {
    return (
      <View >
        <LinearGradient  colors={['#F05A28', '#EF4036']} style={styles.background } >
        {/* <View style={styles.head}> 
        <Text style={styles.head1}> lsdaflk </Text>
        <Text style={styles.head2}> lsdaflk </Text>        
         </View> */}
        <TextInput
        style={styles.textinput1}
        onChangeText={(text) => this.setState({text1})}
        placeholder={this.state.text1}
        placeholderTextColor={'white'}
      />
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text2})}
        placeholder={this.state.text2}
        placeholderTextColor={'white'}
      ></TextInput>
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text1})}
        placeholder={this.state.text1}
        placeholderTextColor={'white'}
      />
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text2})}
        placeholder={this.state.text2}
        placeholderTextColor={'white'}
      ></TextInput>
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text1})}
        placeholder={this.state.text1}
        placeholderTextColor={'white'}
      />
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text2})}
        placeholder={this.state.text2}
        placeholderTextColor={'white'}
      ></TextInput>
      <TouchableOpacity  onPress={()=>this.props.navigation.navigate('Subscribe')} >
        <Image style={styles.buttonin}    source={require('./UI/White.png')}
    
        />
        </TouchableOpacity>
        
        </LinearGradient>
      </View>
    );
  }
}

export default Signup;

const styles = StyleSheet.create({


    background:{
        width:'100%',
        height:'100%',
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
    },
    textinput1 :{
        borderBottomColor: 'white', borderBottomWidth: 1,alignSelf: 'center',width:'60%',marginTop: '50%', color:'white',

    },
    textinput2 :{
         borderBottomColor: 'white', borderBottomWidth: 1,alignSelf: 'center',width:'60%', marginTop: '2%',color:'white',

    },
    buttonin:{
       width:50, height:50,
        alignSelf:'center',
        marginTop: 20,
        marginLeft:200,
    },
    head:{
        height:100,
        alignSelf:'flex-start',
        flex: 1,
        flexDirection: 'row',
        marginTop:20,
    },
    head1:{
        alignSelf:'flex-start',
        marginLeft: 20,
        
        color:'white',
        fontSize:20,
    },
    head2:{
        alignSelf:'flex-end',
        marginRight: 20,
        
        color:'white',
        fontSize:20,
    }
})