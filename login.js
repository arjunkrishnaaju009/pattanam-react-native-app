import React, { Component } from 'react';
import { View, Text,Image,StyleSheet,TextInput,TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        text1: 'Username',
        text2: 'Password'
    };
  }

  render() {
   
    return (
      <View style={styles.root}
      
      >
      
      <Image
          source={require('./UI/Curve.png')}
          style={styles.image}
        />
        <TextInput
        style={styles.textinput1}
        onChangeText={(text) => this.setState({text1})}
        placeholder={this.state.text1}
      />
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text2})}
        placeholder={this.state.text2}
      ></TextInput>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('Subscribe')}>
     
      <Image
    source={require('./UI/Login.png')}
    style={styles.buttonin}
        />
        </TouchableOpacity>
     <View>
         <TouchableOpacity onPress={()=>this.props.navigation.navigate('Signup')}>
      <Text style={styles.signup1}>Don't you have an account ? <Text style={styles.signup2}>Sign Up</Text></Text>
      </TouchableOpacity> 
      </View>
      
       
      </View>


    );
  }
}

export default login;
const styles = StyleSheet.create({
    root: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    image:{

width:'100%',
height:'45%'
    },
    textinput1 :{
        borderBottomColor: 'gray', borderBottomWidth: 1,alignSelf: 'center',width:'60%',marginTop: '30%',

    },
    textinput2 :{
         borderBottomColor: 'gray', borderBottomWidth: 1,alignSelf: 'center',width:'60%', marginTop: '2%',

    },

    buttonin:{

marginTop:'20%', width:60, height:60,alignSelf: 'center',



   


    },
    signup1:{
        color:'#808080',
        alignSelf: 'center',
        marginTop: 5,

    },
    signup2:{
        color:'#EF4036',
        alignSelf: 'center',
        marginTop: 5,
        fontWeight:'bold',

    },
    linearGradient:{
        height:40,
        width:'100%'
    }
   
})