import React, { Component } from 'react';
import { View, Text,StyleSheet ,Image,TextInput,TouchableOpacity} from 'react-native';

class First extends Component {
  constructor(props) {
    super(props);
    this.state = {
        text1: 'Phone number',
        text2: 'Hometown'
    };
  }

  render() {
    return (
      <View style={styles.viewmain}>
        <TextInput
        style={styles.textinput1}
        onChangeText={(text) => this.setState({text1})}
        placeholder={this.state.text1}
      />
      <TextInput
        style={styles.textinput2}
        onChangeText={(text) => this.setState({text2})}
        placeholder={this.state.text2}
      ></TextInput>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
     
      <Image
    source={require('./UI/ICONOrange.png')}
    style={styles.buttonin}
        />
        </TouchableOpacity>
      </View>
    );
  }
}

export default First;

const styles=StyleSheet.create({
viewmain:{
alignContent: 'center',

},
    textinput1 :{
        borderBottomColor: 'gray', borderBottomWidth: 1,alignSelf: 'center',width:'60%',marginTop: '70%',
alignSelf:'center'
    },
    textinput2 :{
         borderBottomColor: 'gray', borderBottomWidth: 1,alignSelf: 'center',width:'60%', marginTop: '2%',

    },
    buttonin:{

        marginTop:10, width:60, height:60,alignSelf: 'flex-end', marginRight: '20%',
      },
})