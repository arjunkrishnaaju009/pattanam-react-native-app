/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { createStackNavigator,createAppContainer } from 'react-navigation';
import Login from './login';
import Signup from './signup';
import Subscribe from './Subscribe';
import First from './First';


const RootStack = createStackNavigator({
  First:{
    screen:First,
    navigationOptions:{
      header:null,
    }
  },
  Login: {
    screen: Login,
    
    navigationOptions: {
      header: null
    } },
    Signup: {
      screen: Signup,
      navigationOptions: {
        header: null
      } },
      Subscribe :{
        screen: Subscribe,
        navigationOptions:{
          header:null
        }
        

      },
      
      
  initialRouteName: 'First'
});






class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const Root = createAppContainer(RootStack);
    return (
      <Root />
     
    );
  }
}

export default App;
