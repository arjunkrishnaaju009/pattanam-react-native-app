import React, { Component } from 'react';
import { View, Text,StyleSheet,ScrollView ,Image,ImageBackground} from 'react-native';

class Subscribe extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  
  render() {
    return (
      <View style={styles.viewmain}>
      <View style={styles.viewmain2}>
        <Text style={styles.Heading1}>Subscribe</Text>
        <Text style={styles.Heading2}>Taste your own Hometown</Text>
<ScrollView style={styles.scroll} >
    <View style={styles.viewsub1}>
    
    <ImageBackground imageStyle={{ borderRadius: 90 }} style={styles.images} source={require('./UI/health.png')} >
        <Text style={styles.text1}>HEALTH TIPS</Text>
    </ImageBackground>
    
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images1} source={require('./UI/Technology.png')} >
    </ImageBackground>
    
    </View>
    <View style={styles.viewsub}>
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images} source={require('./UI/Movie.png')} >
    </ImageBackground>
    
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images1} source={require('./UI/Vehicles.png')} >
    </ImageBackground>
    
    </View>
    <View style={styles.viewsub}>
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images} source={require('./UI/Motivation.png')} >
    </ImageBackground>
    
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images1} source={require('./UI/Business.png')} >
    </ImageBackground>
    
    </View>
    <View style={styles.viewsub}>
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images} source={require('./UI/Education.png')} >
    </ImageBackground>
    
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images1} source={require('./UI/Sports.png')} >
    </ImageBackground>
    
    </View>
    
    <View style={styles.viewsub2}>
    <ImageBackground imageStyle={{ borderRadius: 90 }}  style={styles.images} source={require('./UI/PSC.png')} >
    </ImageBackground>
    
    <ImageBackground imageStyle={{ borderRadius: 90 }} style={styles.images1} source={require('./UI/Recepie.png')} >
    </ImageBackground>
    
    </View>
   

    
</ScrollView>
</View>
<View style={styles.end}>
<Image style={styles.buttonin}    source={require('./UI/White.png')} />
</View>
      </View>
    );
  }
}

export default Subscribe;

const styles= StyleSheet.create({

viewmain:{
    
    height:'100%',
    width:'100%',
    flex:1,
    
},
viewmain2:{
    marginHorizontal:20,
    flex:1,
    
},
    Heading1:{
        fontSize:30,
        marginTop: 30,
        

    },
    Heading2:{
        fontSize:20,
        color:'#808080',
        
        

    },
    images:{
        width: 180,
        height:180,
        borderRadius: 90,
        alignItems: 'flex-end',
        
        
    },
    images1:{
        width: 180,
        height:180,
        borderRadius: 90,
        marginLeft: 20,
        alignItems:'center',
        
    },
    viewsub:{
        flexDirection:'row',
        marginTop:20
    },
    viewsub1:{
        flexDirection:'row',
        marginTop:20
    },
    viewsub2:{
        flexDirection:'row',
        marginBottom: 20,
        marginTop: 20,
    },
    end:{
        height:70,
        width:'100%',
        backgroundColor: '#EF4036',
           alignSelf: 'flex-end',   
        
    },
    buttonin:{

         width:50, height:50,alignSelf: 'flex-end', marginTop:10, marginBottom: 10, marginRight: '10%',

            },
    scroll:{
                flex:1,
            height:'100%'
        },
subtext:{
fontSize:25,
color:'white',
flexWrap: 'wrap',
justifyContent:'center',
marginTop:'50%',
}, 
    text1:{
        fontSize:25,
        color:'white',
        width:100,
        height:100,
        flexWrap:'wrap',
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent: 'center',
    },     

})